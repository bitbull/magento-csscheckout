// Load plugins
var
    gulp         = require('gulp'),
    less         = require('gulp-less'),
    minifycss    = require('gulp-minify-css'),
    uglify       = require('gulp-uglify'),
    notify       = require('gulp-notify')

//config | false for debug

var config = {

    minifyCss: false,
    uglifyJS: false

}

gulp.task('default', ['css', 'watch']);

// Compile less | minify if config is true | notify

gulp.task('css', function() {
    return gulp.src('csscheckout.less')
        .pipe(less().on('error', notify.onError(function (error) {
            return 'Error compiling LESS: ' + error.message;
        })))
        .pipe(minifycss())
        .pipe(gulp.dest('css'))
        .pipe(notify({
            message: 'Successfully compiled LESS'
        }));
});

// Watch .less files

gulp.task('watch', function() {
    gulp.watch('csscheckout.less', ['css']);
});