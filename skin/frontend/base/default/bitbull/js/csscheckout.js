/**
 * Created by irene on 18/06/15.
 */

document.observe("dom:loaded", function() {

    [].forEach.call(document.querySelectorAll('.opc'), function (step) {
        var step = (step.childNodes[1].childNodes[3]).offsetHeight;
        var height = step + 108;

        console.log(height);
        document.getElementById('checkoutSteps').setAttribute('style','height:'+ height +'px');

    });

    //Chekout Steps

    Checkout.prototype.gotoSection = function (section, reloadProgressBlock) {
        if (reloadProgressBlock) {
            this.reloadProgressBlock(this.currentStep);
        }
        this.currentStep = section;

        var sectionElement = $('opc-' + section);
        var stepcontent = document.getElementById('checkout-step-' + section );
        var height = stepcontent.offsetHeight  + 280;


        sectionElement.addClassName('allow');
        this.accordion.openSection('opc-' + section);

        if (!reloadProgressBlock) {
            this.resetPreviousSteps();
        }

        document.getElementById('checkoutSteps').setAttribute('style','height:'+ height + 'px' );
    };

   // New address

    Billing.prototype.newAddress = function (isNew) {
        if (isNew) {
            this.resetSelectedAddress();
            Element.show('billing-new-address-form');
            document.getElementById('checkoutSteps').setAttribute('style','height: 900px' );
        } else {
            Element.hide('billing-new-address-form');
            document.getElementById('checkoutSteps').setAttribute('style','height: 450px' );
        }
    };

});


