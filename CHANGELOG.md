# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## Unreleased

# Version v1.1.0
- Edited modman file to use a common path of other bitbull modules

# Version v.1.0.0
- Added first tag to the master